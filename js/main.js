load_list();

function load_list(){
  get("inventory.lst",function(items){
    items = items.split("\n");
    let list = document.querySelector("#list");
    list.innerHTML = "";

    for( item of items ){
      let i = item.split("|")[2];
      let qty = item.split("|")[0];
      if ( i != ""){
        list.innerHTML += `<div class="well items" onclick="select_item('${i}','${qty}')">[${qty}] ${i}</div>`;
      }
    }
  })
}

function select_item(item,qty){
  let title = `Ordering ${item} Minimum Required ${qty}`;
  let msg = `
  How Many ${item} Would You Like?
  <br><input id="amount" type="number" value="1">
  `;
  let footer = `<button class="button" onclick="order('${item}')">ORDER</button>`;
  showModal(title,msg,footer);
  let amount = document.querySelector("#amount");
  amount.focus();
}

function order(item){
  let amount = document.querySelector("#amount").value;
  let output = document.querySelector("#output");
  let input = document.querySelector("#input");
  hideModal();
  output.innerHTML = `${amount} ${item} has been ordered`;
  input.value = "";  
  let items = document.querySelectorAll(".items"); 
  for( item of items ){ 
      item.style.display = "";
      item.classList.add("vitem");
      item.classList.remove("hidden");
  }
  input.focus();
}

let filter_input = document.querySelector("#input");
filter_input.focus();
filter_input.addEventListener('input', filter);

function filter(){
  let items = document.querySelectorAll(".items");
  let q = filter_input.value.toUpperCase();
  for( item of items ){
    if(item.innerHTML.toUpperCase().includes(q)){
      //item.style.display = "";
      item.classList.add("vitem");
      item.classList.remove("hidden");
    }else{
      //item.style.display = "none";
      item.classList.add("hidden");
      item.classList.remove("vitem");
    }
  }
  let vitems = document.querySelectorAll(".vitem");
  let count = vitems.length;
  if(count == 1 ){
    vitems[0].click();
  }
}

// example filter list 1
/*
let listItems = ["John Smith", "Sam Johnson", "Sally Sims", "Jack Packer", "Tim Sox"];
createFilter_1("list","theList", "items", listItems,"Filter Names...");
let items = document.querySelector("#list").querySelectorAll("li");
for( i of items ){
  i.classList.add("name");
  i.addEventListener("click", itemClick);
}


let colors = ["red","blue","green","yellow","purple"];
createFilter_1("colors","thecolors", "citems", colors, "Filter Colors...");
let coloritems = document.querySelector("#colors").querySelectorAll("li");
for( i of coloritems ){
  i.classList.add("colors");
  i.addEventListener("click", itemClick);
}

function itemClick(){
  console.log(this.innerText);
}
*/

// example request
/*
get('get.php', function(data){ 
  console.log(data); 
});
*/

/*
get('get_json.php', function(data){ 
  data = JASON.parse(data);
  for( d of data ){
    console.log(d);
  }
});
*/

// example request
/*
post('post.php', 'p1=1&p2=Hello+World', function(data){
  console.log(data); 
});
*/
// example request with data object
/*
post('post.php', { p1: 1, p2: 'Hello World' }, function(data){ 
  console.log(data); 
});
*/
