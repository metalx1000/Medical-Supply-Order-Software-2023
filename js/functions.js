
function get(url, success) {
  var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
  xhr.open('GET', url);
  xhr.onreadystatechange = function() {
    if (xhr.readyState>3 && xhr.status==200) success(xhr.responseText);
  };
  xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  xhr.send();
  return xhr;
}

function postAjax(url, data, success) {
  var params = typeof data == 'string' ? data : Object.keys(data).map(
    function(k){ return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) }
  ).join('&');

  var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
  xhr.open('POST', url);
  xhr.onreadystatechange = function() {
    if (xhr.readyState>3 && xhr.status==200) { success(xhr.responseText); }
  };
  xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.send(params);
  return xhr;
}


function listFilter_1(input,listid) {
  var filter, ul, li, a, i, txtValue;
  filter = input.value.toUpperCase();
  ul = document.getElementById(listid);
  li = ul.getElementsByTagName("li");
  for (i = 0; i < li.length; i++) {
    a = li[i].getElementsByTagName("a")[0];
    txtValue = a.textContent || a.innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      li[i].style.display = "";
    } else {
      li[i].style.display = "none";
    }
  }
}

function createFilter_1(output_box_id,input_id,list_id,items,place_holder){
  let output_box = document.querySelector(`#${output_box_id}`);
  output_box.innerHTML = `
  <input type="text" id="${input_id}" class="filterInput" onkeyup="listFilter_1(this,'${list_id}')" placeholder="${place_holder}">

<ul id="${list_id}" class="filterList">
</ul>`

  let list = document.querySelector(`#${list_id}`);
  for( i of items ){
    list.innerHTML += `<li class="listItem"><a href="#">${i}</a></li>`;
  }
}
