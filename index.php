<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/main.css" />
  </head>
  <body>
    <div class="flex">
      <input id="input" type="text" placeholder="Filter List"/>
    </div>
    <div id="output">Status</div>
    <div id="list"></div>
  </body>
  <script src="js/functions.js"></script>
  <script src="js/main.js"></script>
<?php include("php/modal.php");?>
</html>

